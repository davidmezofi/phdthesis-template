version = v0.1.1
tarname = $(addsuffix $(version).tar.gz, phdthesis-)
chapters = $(wildcard frontback/*.tex) $(wildcard chapters/*.tex)
auxfiles = $(addprefix phdthesis., aux bbl blg fdb_latexmk fls log toc out loa lol lot)
src = src/snippet.sh
deps = preamble.tex commands.tex references.bib $(src)

all: phdthesis.pdf

phdthesis.pdf: phdthesis.tex $(deps) $(chapters)
	latexmk -bibtex -pdf $<

release: phdthesis.pdf $(src)
	tar -czvf $(tarname) $^ 

clean:
	rm -f $(auxfiles)

cleanall: clean
	rm -f phdthesis.pdf

.PHONY: all clean cleanall release
