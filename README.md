Ph.D. thesis template
===================

- I would recommend to use at most 2 levels of heading: chapters and sections.
  Why? This way theorems, definitions, etc. will be numbered like X.Y.Z. For
  me, more than 3 numbers looks confusing.

- Instead of subsections you could use the `minisec` command provided by the
  class `scrartcl`.

- Write your text into the dedicated LaTeX files under the directories
  `chapters` and `frontback`

- Put your source code you would like to include in the thesis into `src`.
  Using listings you can show these snippets in the text nicely formatted.

- You can change the header of `Listings` to anything you want, see
  `preamble.tex`.

- By default the date of the compilation and the version number of your thesis
  (which I recommend to use, also use some version control system) in place of
  the date on the cover, and in the headers. It could help collaboration with
  your supervisor. You can switch this off at the end of `preamble.tex`.

- There is a `Makefile` included. Using `make` makes the compilation of LaTeX
  files less cumbersome. Use `make clean` to get rid of the auxiliary files,
  `make cleanall` deletes the generated PDF as well. `make release` bundles
  your PDF and some source files you set into a tar.

- Put your university crest under `grphx` if you would like to use it, and
  alter the `mycrest` command accordingly.

- Feel free to clone this repository, and I hope it will be useful for the
  other PhD students out in the wild.

- I took some ideas from the excellent LaTeX template bundle
  [`classicthesis`][classicthesis].


[classicthesis]: https://bitbucket.org/amiede/classicthesis/wiki/Home
